import { defineStore } from 'pinia'
import { nextTick, ref, watch } from 'vue'
import { useRouter } from 'vue-router'
export const useMenuStore = defineStore('menu', {
    state: () => {
        return {
            menuItems: [{ path: '/home', name: '首页' }],
            router:useRouter(),
        }
    },
    actions: {
        async removeMenuItem(val) {   
            const router = useRouter()
            for (let i = 0; i < this.menuItems.length; i++){
                if (val === this.menuItems[i].path && this.menuItems.length>1) {
                    try {
                        this.menuItems.splice(i, 1) 
                        this.router.push({
                            path:this.menuItems[i-1]?this.menuItems[i-1].path:this.menuItems[i].path 
                        })
                     
                    } catch (error) {
                        console.log(error);
                    }
                }
            }
        }
    },
}) 
