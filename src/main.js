import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
//引入ElementPlus
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import * as ElIcons from "@element-plus/icons-vue";
import { createPinia } from "pinia";

import axios from "axios";

let mockService;
if (process.env.NODE_ENV === "development") {
  // 开发环境下引入mock服务
  mockService = require("./mock/index.js").default; // 或 import './mock/index.js';
}
// 如果不想用mock测试就把这行代码注释掉
import "./mock/index.js";

const app = createApp(App);

//引入echarts
import * as echarts from "echarts";

// 全局注册
for (const name in ElIcons) {
  app.component(name, ElIcons[name]);
}
// //使用ElementPlus
app.use(ElementPlus).use(store).use(router).mount("#app");
app.use(createPinia());
app.provide("$axios", axios);
//vue全局注入echarts
app.provide("$echarts", echarts);
