import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/MainView.vue";

const routes = [
  {
    path: "/",
    name: "main", //主页
    redirect: "home",
    component: () => import("../views/MainView.vue"),
    children: [
      {
        path: "/home",
        name: "首页", //首页
        component: () => import("../views/HomePage/HomePage.vue"),
      },
      {
        path: "/SingleColumn",
        name: "单列表单", //单列表单
        component: () => import("../views/FormManage/SingleColumnForm.vue"),
      },
      {
        path: "/MultiColumn",
        name: "多列表单", //多列表单
        component: () => import("../views/FormManage/MultiColumnFrom.vue"),
      },
      {
        path: "/Grouping",
        name: "分组表单", //分组表单
        component: () => import("../views/FormManage/GroupingFrom.vue"),
      },
      {
        path: "/Tabs",
        name: "标签页表单", //标签页表单
        component: () => import("../views/FormManage/TabsFrom.vue"),
      },
      {
        path: "/MasterSlave",
        name: "主从表单", //主从表单
        component: () => import("../views/FormManage/MasterSlaveFrom.vue"),
      },
      {
        path: "/Responsive",
        name: "响应式表单", //响应式表单
        component: () => import("../views/FormManage/ResponsiveFrom.vue"),
      },
      {
        path: "/QuestionnaireSurvey",
        name: "问卷调查表单", //问卷调查表单
        component: () =>
          import("../views/FormManage/QuestionnaireSurveyFrom.vue"),
      },
      {
        path: "/FixedTable",
        name: "固定表格表单", //固定表格表单
        component: () => import("../views/FormManage/FixedTableFrom.vue"),
      },
      {
        path: "/User",
        name: "用户管理", //用户管理
        component: () => import("../views/system/User.vue"),
      },
      {
        path: "/Menu",
        name: "菜单管理", //菜单管理
        component: () => import("../views/system/Menu.vue"),
      },
      {
        path: "/Role",
        name: "权限管理", //权限管理
        component: () => import("../views/system/Role.vue"),
      },
    ],
  },
  {
    path: "/login",
    name: "登录", //登录
    component: () => import("../views/LoginView.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  mode: "hash",
  // history: createWebHistory(process.env.BASE_URL),
  routes,
});

// 挂载路由导航守卫
// router.beforeEach((to, from, next) => {
//   if (to.path === "/login") return next();
//   // 获取token
//   const tokenStr = window.sessionStorage.getItem("token");
//   if (!tokenStr) return next("/login");
//   next();
// });
export default router;
