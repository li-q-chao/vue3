import axios from "axios";
import Mock from "mockjs";
Mock.mock("/menu", {
  code: 200,
  message: "获取成功",
  data: [
    {
      icon: "House",
      url: "home",
      title: "首页",
    },
    {
      icon: "Document",
      url: "SingleColumn",
      title: "表单管理",
      children: [
        {
          url: "SingleColumn",
          title: "单列表单",
          icon: "Document",
        },
        {
          url: "MultiColumn",
          title: "多列表单",
          icon: "Document",
        },
        {
          url: "Grouping",
          title: "分组表单",
          icon: "Document",
        },
        {
          url: "Tabs",
          title: "标签页表单",
          icon: "Document",
        },
        {
          url: "MasterSlave",
          title: "主从表单",
          icon: "Document",
        },
        {
          url: "Responsive",
          title: "响应式表单",
          icon: "Document",
        },
        {
          url: "QuestionnaireSurvey",
          title: "问卷调查表单",
          icon: "Document",
        },
        {
          url: "FixedTable",
          title: "固定表格表单",
          icon: "Document",
        },
      ],
    },
    {
      icon: "Stamp",
      url: "person",
      title: "系统管理",
      children: [
        {
          url: "User",
          title: "用户管理",
          icon: "House",
        },
        {
          url: "Role",
          title: "权限管理",
          icon: "House",
        },
        {
          url: "Menu",
          title: "菜单管理",
          icon: "House",
        },
      ],
    },
  ],
});
Mock.mock("/carte", {
  code: 200,
  message: "获取成功",
  data: [
    {
      icon: "Document",
      url: "SingleColumn",
      title: "表单管理",
      children: [
        {
          url: "SingleColumn",
          title: "单列表单",
          icon: "Document",
        },
        {
          url: "MultiColumn",
          title: "多列表单",
          icon: "Document",
        },
        {
          url: "Grouping",
          title: "分组表单",
          icon: "Document",
        },
        {
          url: "Tabs",
          title: "标签页表单",
          icon: "Document",
        },
        {
          url: "MasterSlave",
          title: "主从表单",
          icon: "Document",
        },
        {
          url: "Responsive",
          title: "响应式表单",
          icon: "Document",
        },
        {
          url: "QuestionnaireSurvey",
          title: "问卷调查表单",
          icon: "Document",
        },
        {
          url: "FixedTable",
          title: "固定表格表单",
          icon: "Document",
        },
      ],
    },
    {
      icon: "Stamp",
      url: "person",
      title: "系统管理",
      children: [
        {
          url: "User",
          title: "用户管理",
          icon: "House",
        },
        {
          url: "Role",
          title: "权限管理",
          icon: "House",
        },
        {
          url: "Menu",
          title: "菜单管理",
          icon: "House",
        },
      ],
    },
  ],
});
Mock.mock("/user", {
  code: 200,
  message: "获取成功",
  data: [
    {
      username: "admin",
      password: "123456",
      role: "管理员",
      serial_number: "1",
      token: "admin",
      Image:
        "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif",
      introduction: "I am a super administrator",
      status: "启用",
      createTime: "2021-01-01 00:00:00",
      updateTime: "2021-01-01 00:00:00",
    },
    {
      username: "admin",
      password: "123456",
      role: "测试员",
      serial_number: "2",
      token: "admin",
      Image:
        "https://s1.aigei.com/src/img/gif/b2/b2d7228da575409fae5ea016bb31d737.gif?imageMogr2/auto-orient/thumbnail/!282x282r/gravity/Center/crop/282x282/quality/85/%7CimageView2/2/w/282&e=1735488000&token=P7S2Xpzfz11vAkASLTkfHN7Fw-oOZBecqeJaxypL:SMxQwPu9XOo7txy62LQ9jMaOZQA=",
      introduction: "I am a super administrator",
      status: "启用",
      createTime: "2021-01-01 00:00:00",
      updateTime: "2021-01-01 00:00:00",
    },
  ],
});
Mock.mock("/role", {
  code: 200,
  message: "获取成功",
  data: [
    {
      role_number: "1",
      role_name: "超级管理员",
      authority_character: "admin",
      display_order: "1",
      status: "1",
      super: "1",
      createTime: "2024-04-21 18:20:30",
      updateTime: "",
    },
    {
      role_number: "2",
      role_name: "普通角色",
      authority_character: "common",
      display_order: "2",
      status: "1",
      super: "2",
      createTime: "2024-04-21 18:22:23",
      updateTime: "",
    },
    {
      role_number: "2",
      role_name: "普通角色",
      authority_character: "common",
      display_order: "2",
      status: "1",
      super: "2",
      createTime: "2024-04-21 18:22:23",
      updateTime: "",
    },
    {
      role_number: "2",
      role_name: "普通角色",
      authority_character: "common",
      display_order: "2",
      status: "1",
      super: "2",
      createTime: "2024-04-21 18:22:23",
      updateTime: "",
    },
    {
      role_number: "2",
      role_name: "普通角色",
      authority_character: "common",
      display_order: "2",
      status: "1",
      super: "2",
      createTime: "2024-04-21 18:22:23",
      updateTime: "",
    },
    {
      role_number: "2",
      role_name: "普通角色",
      authority_character: "common",
      display_order: "2",
      status: "1",
      super: "2",
      createTime: "2024-04-21 18:22:23",
      updateTime: "",
    },
  ],
});
