import request from "@/utils/request.js";

//查询所有用户权限
export function Query(data) {
  return request({
    url: "/role",
    method: "get",
    data,
  });
}
//添加用户权限
export function Addition(data) {
  return request({
    url: "/role",
    method: "post",
    data,
  });
}
//修改用户权限
export function Modify(data) {
  return request({
    url: "/role",
    method: "patch",
    data,
  });
}
//删除用户权限
export function Delete(data) {
  return request({
    url: "role",
    method: "delete",
    data,
  });
}
