import request from "@/utils/request.js";
//用户列表
export function user() {
  return request({
    url: "/user",
    method: "get",
  });
}

