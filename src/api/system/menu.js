import request from "@/utils/request.js";
//菜单列表
export function menu() {
  return request({
    url: "/menu",
    method: "get",
  });
}
//菜单列表
export function carte() {
  return request({
    url: "/carte",
    method: "get",
  });
}
