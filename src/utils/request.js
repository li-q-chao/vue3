import axios from "axios";
import { ElLoading, ElMessage } from "element-plus";

let service = axios.create({
  baseURL: "", //本地
  // 超时时间 单位是ms，这里设置了3s的超时时间
  timeout: 3 * 1000,
});

let loadingInstance;

// 拦截器的添加
service.interceptors.request.use(
  (config) => {
    loadingInstance = ElLoading.service("加载中");
    return config;
  },
  (err) => {
    loadingInstance?.close();
    ElMessage.error("网络异常");
    return Promise.reject(err);
  }
);

//响应拦截器
service.interceptors.response.use(
  (res) => {
    loadingInstance?.close();
    return res.data;
  },
  (err) => {
    loadingInstance?.close();
    ElMessage.error("请求失败");
    return Promise.reject(err);
  }
);
export default service;
