const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  devServer: {
    proxy: {
      '/api': {                           // /api是习惯性的写法，可以随意改
        target: 'http://localhost:8081/', //接口域名
        changeOrigin: true,               //是否跨域
        ws: true,                         //是否代理 websockets
        secure: false,                    //是否https接口
        pathRewrite: {                    //路径重置
          '^/api': ''                     // 删除请求路径中的'/api'前缀
        }
      }
    }
  }
})
