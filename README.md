# demo

## 下载项目依赖
```
npm install
```

### 开始运行
```
npm run serve
```

### 进行打包
```
npm run build
```

### 整理和修复文件爱你
```
npm run lint
```

### 自定义配置去往官方文档地址
See [Configuration Reference](https://cli.vuejs.org/config/).
